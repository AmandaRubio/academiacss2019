function openNav() {
    if (window.matchMedia("(min-width: 800px").matches) {
        document.getElementById("mySidenav").style.width = "340px";
        document.getElementById("mySidenav").style.height = "auto";
        document.getElementById("main").style.marginLeft = "340px";
    } else {
        document.getElementById("mySidenav").style.width = "auto";
        document.getElementById("mySidenav").style.height = "auto";
        document.getElementById("main").style.marginLeft = "0";
    }
}
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("mySidenav").style.height = "0";
    document.getElementById("main").style.marginLeft = "0";
}